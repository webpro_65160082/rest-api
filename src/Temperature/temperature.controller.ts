import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { TempService } from './temperature.service ';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly tempService: TempService) {}

  @Get('query')
  query(@Query('celsius') celsius: number, @Query('type') type: string) {
    return { celsius: celsius, type: type };
  }

  @Get('param/:celsius')
  param(@Req() req, @Param('celsius') celsius: number) {
    return { celsius };
  }
  @Post('body')
  body(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return celsius;
  }

  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.tempService.convert(parseFloat(celsius));
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.tempService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.tempService.convert(celsius);
  }
}
