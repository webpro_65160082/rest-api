import { Controller, Delete, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  @Delete('hi')
  getHi(): string {
    return '<html><body><h1>Hi</h1></body></html>';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello</h1></body></html>';
  }
}
