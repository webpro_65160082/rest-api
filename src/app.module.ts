import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TempModule } from './Temperature/temperature.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TempModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
